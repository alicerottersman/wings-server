(ns wings-auth.handler
  (require [wings-auth.service :as service]))

(defn create-auth-token [request]
  (let [[ok? results] (service/create-auth-token (:auth-config request)
                                                 (:params request))]
    (if ok?
      {:status 201 :body results}
      {:status 401 :body results})))

(defn create-user [request]
;all users created in this manner are just users, not admin
  (try (service/add-user
        (assoc  (:params request) :user-roles [2]))
       {:status 201}
       (catch Exception e   
         {:status 404 :body e})))
  
