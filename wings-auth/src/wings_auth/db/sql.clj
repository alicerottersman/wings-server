(ns wings-auth.db.sql
  (:require [yesql.core :refer [defqueries]]
            [wings-auth.db.specs :as db]))

(defqueries "queries/user_queries.sql" {:connection db/mariadb-spec})
(defqueries "queries/user_role_queries.sql" {:connection db/mariadb-spec})
(defqueries "queries/refresh_token_queries.sql" {:connection db/mariadb-spec})
