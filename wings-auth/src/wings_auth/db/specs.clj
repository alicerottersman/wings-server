(ns wings-auth.db.specs)
(def mariadb-spec
  {:connection-uri (System/getenv "AUTH_DB_URL")})
