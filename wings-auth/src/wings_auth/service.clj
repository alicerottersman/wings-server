(ns wings-auth.service
  (:require [wings-auth.db.sql :as sql]
            [buddy.hashers :as hash]
            [buddy.sign.jws :as jws]
            [buddy.sign.util :as util]
            [buddy.core.keys :as keys]
            [clj-time.core :as time]
            [clojure.java.io :as io]
            [hashids.core :as hashids]))

(def hashids-opts {:salt "wings for the bros & hoes"}) 

(defn add-user [user]
  "Create a user with an email, password, and a list of roles, returns user id"
  (let [user-id
        ((sql/add-user<! {:email (user :email)
                          :password (hash/encrypt (user :password))})
         :insert_id)]
    (doseq [user-role (:user-roles user)]
        (sql/create-user-role<! {:user_id user-id, :role_id user-role}))
    user-id)) 

(defn count-users []
  ((first (sql/count-users)) :count))

(defn count-user-roles []
  ((first (sql/count-user-roles)) :count))

(defn count-refresh-tokens []
  ((first (sql/count-refresh-tokens)) :count))

(defn find-user-by-id [id]
  (first (sql/find-user-by-id {:id id})))

(defn find-user-by-email [email]
  (first (sql/find-user-by-email {:email email})))

(defn find-refresh-token [user-id, iat]
  (first (sql/find-unique-token {:user_id user-id :issued iat})))

(defn find-all-users []
  (sql/find-all-users))

(defn find-all-user-roles []
  (sql/find-all-user-roles))

(defn update-user [user]
  (sql/update-user! (update-in user [:password] #(hash/encrypt %))))

(defn delete-user [id]
  (sql/delete-user! {:id id}))

(defn delete-all-users []
  (sql/delete-all-users!))

(defn delete-all-refresh-tokens []
  (sql/delete-all-refresh-tokens!))

(defn authenticate-user [credentials]
  (let [user (find-user-by-email (:email credentials))]
    (if user
      (if (hash/check (:password credentials) (:password user))
        [true {:user (dissoc user :password)}]
        [false {:message "Invalid password."}])
      [false {:message "Invalid email."}])))

(defn- private-key [auth-config]
  (keys/private-key
   (io/resource (:private-key auth-config))
   (:passphrase auth-config)))

(defn- public-key [auth-config]
  (keys/public-key
   (io/resource (:public-key auth-config))))

(defn- unsign-token [auth-config token]
"Unsign a token using the auth-config's public key. 
The buddy call jws/unsign throws an exception when 
the message is corrupt -it's wrapped here to just
return false when such occurs."
  (try
    (jws/unsign token (public-key auth-config) {:alg :rs256})
    (catch Exception e
        false)))


(defn- make-access-token [auth-config user]
  (let [expiration (-> (time/plus (time/now) (time/minutes 30))
                       (util/to-timestamp))]
    (jws/sign {:user (assoc (dissoc user :password)
                       :public-id (hashids/encode hashids-opts (user :id)))}
              (private-key auth-config)
              {:alg :rs256 :exp expiration})))

(defn- make-refresh-token [auth-config user]
  (let [now (util/to-timestamp (time/now))
        token (jws/sign {:user-id  (:id user)}
                        (private-key auth-config)
                        {:alg :rs256
                         :iat now
                         :exp (-> (time/plus (time/now) (time/days 30))
                                  (util/to-timestamp))})]
    (sql/add-refresh-token! {:user_id (:id user)
                             :issued now
                             :token token})
    token))

(defn make-token-pair [auth-config user]
  {:token-pair {:auth-token (make-access-token auth-config user)
                :refresh-token (make-refresh-token auth-config user)}})

(defn create-auth-token [auth-config credentials]
  (let [[ok? results] (authenticate-user credentials)]
    (if ok?
      [true (make-token-pair auth-config (:user results))]
      [false results])))

(defn refresh-auth-token [auth-config refresh-token]
  (if-let [unsigned (unsign-token auth-config refresh-token)]
    (let [token-db-record (find-refresh-token (:user-id unsigned)
                                              (:iat unsigned))
          user (find-user-by-id (:user_id token-db-record))]
      (if (= 1 (:valid token-db-record)) 
        (do
          (sql/invalidate-token-with-id! {:id (:id token-db-record)})
          [true (make-token-pair auth-config user)])
        [false {:message "Refresh token revoked/deleted, or new refresh token already created!"}]))
    [false {:message "Invalid or expired refresh token provided."}]))

(defn invalidate-refresh-token [auth-config refresh-token]
  (let [unsigned (unsign-token auth-config refresh-token)]
    (if unsigned
      (do
        (sql/invalidate-token! {:user_id (:user-id unsigned)
                                :issued (:issued unsigned)})
        [true {:message "Invalidated successfully!"}]))
    [false {:message "Invalid or expired refresh token"}]))
