(ns wings-auth.core
  (:use compojure.core)
  (:require [ragtime.jdbc :as jdbc]
            [ragtime.repl :as repl]
            [environ.core :refer [env]]
            [ring.util.response :refer [response]]
            [wings-auth.handler :as handler]
            [compojure.route :as route]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [ring.middleware.json :refer [wrap-json-response
                                          wrap-json-params]]))

(defn load-config []
  {:datastore  (jdbc/sql-database {:connection-uri  (env :auth-db-url)
                                   :migrations_table "ragtime_migrations"})
   :migrations (jdbc/load-resources "migrations")})
                                        ;zero argument functions for use with lein

(defn migrate []
  (repl/migrate (load-config)))

(defn rollback []
  (repl/rollback (load-config)))

(defroutes app-routes
  (POST "/create-auth-token" [] handler/create-auth-token)
  (POST "/sign-up" [] handler/create-user)
  (route/not-found (response {:message "Not found."})))

(defn wrap-config [handler]
  (fn [request]
    (handler (assoc request :auth-config {:private-key "wings_auth_privkey.pem"
                                          :public-key "wings_auth_pubkey.pem"
                                          :passphrase "l@s@gn@"}))))

(def app
  (-> app-routes
        wrap-config
        wrap-keyword-params
        wrap-json-params
        wrap-json-response))
