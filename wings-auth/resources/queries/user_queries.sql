-- name: add-user<!
-- Creates a user with an email and password
INSERT INTO `user` (`email`,`password`)
VALUES (:email, :password);

-- name: count-users
-- Counts all users
SELECT COUNT(*) as count FROM `user`;

-- name: delete-all-users!
-- Delete all users. For use with testing.
DELETE FROM `user`;

-- name: find-user-by-id
-- Get user by id
SELECT * FROM `user` WHERE `id`=:id

-- name: find-user-by-email
-- Get user by email 
SELECT * FROM `user` WHERE `email`=:email

-- name: find-all-users
-- Get all users
SELECT * FROM `user`;

-- name: update-user!
-- Update the user with matching id
UPDATE `user` SET `email`=:email, `password`=:password
WHERE  `id`=:id;

-- name: delete-user!
-- Delete user with matching id
DELETE FROM `user` WHERE `id`=:id;

