-- name: count-refresh-tokens
-- Count total refresh tokens
SELECT COUNT(*) as count FROM `refresh_token`;

-- name: add-refresh-token!
-- Create a new refresh-token
INSERT INTO `refresh_token` (`user_id`, `issued`, `token`)
VALUES (:user_id, :issued, :token); 

-- name: find-unique-token
-- Finds a unique token by the user's id and issued time
SELECT * FROM `refresh_token`
WHERE `user_id`=:user_id AND `issued`=:issued

-- name: invalidate-token-with-id!
-- Writes invalid to valid column of token with given id
UPDATE `refresh_token` SET `valid`=0
WHERE `id`=:id

-- name: invalidate-token!
-- Writes invalid to valid column of toekn with given user-id and issued date
UPDATE `refresh_token` SET `valid`=0
WHERE `user_id`=:user_id AND `issued`=:issued;

-- name: delete-all-refresh-tokens!
-- Deletes all the refresh tokens
DELETE FROM `refresh_token`;
