-- name: create-user-role<!
-- Creates a user's role for a given user_id and role_id
INSERT INTO `user_role` (`user_id`, `role_id`)
VALUES (:user_id, :role_id);

-- name: count-user-roles
-- Counts the total number of user roles 
SELECT COUNT(*) AS count FROM `user_role`;

-- name: find-user-roles
-- Find a user's roles by a user's id
SELECT r.id, r.application_id
FROM `role` r    
INNER JOIN `user_role` ur on r.id = ur.role_id
WHERE ur.user_id = :user_id

-- name: find-all-user-roles
-- Finds all user roles in table
SELECT * FROM `user_role`;
