CREATE TABLE IF NOT EXISTS `role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(42),
  `application_id` INT NOT NULL,
   PRIMARY KEY (`id`),
   INDEX `application_id_idx`(`application_id` ASC),
  CONSTRAINT `application_id`
    FOREIGN KEY (`application_id`)
    REFERENCES `application`(`id`)
    ON DELETE CASCADE);
