CREATE TABLE IF NOT EXISTS `refresh_token` (
  `issued` DATETIME NOT NULL,
  `token` VARCHAR(255) NOT NULL,
  `user_id` INT NOT NULL,
  `valid` INT NOT NULL DEFAULT 1);
