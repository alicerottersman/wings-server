(ns wings-server.db.sql
  (:require [yesql.core :refer [defqueries]]
            [wings-server.db.specs :as db]))

(defqueries "queries/user_queries.sql" {:connection db/mariadb-spec})
