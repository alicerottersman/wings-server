(ns wings-auth.service-test
  (:use clojure.test
        wings-auth.core-test)
  (:require [wings-auth.service :as service]
            [clj-time.core :as time]))

(defn clean-database [f]
  (service/delete-all-users)
  (service/delete-all-refresh-tokens)
  (f))

(defn- create-test-user
  ([] (create-test-user ""))  
  ([identifier]
   (service/add-user {:email (str "test" identifier "@gmail.com")
                  :password "pass123" :user-roles [1, 2]})))

(use-fixtures :each clean-database)

(deftest create-read-users
  (testing "Creates user with two roles"
    (let [count-orig (service/count-users)
          count-user-role-orig (service/count-user-roles)]
      (create-test-user 0)
      (is (= (inc count-orig) (service/count-users)))
      (is (= (+ 2 count-user-role-orig) (service/count-user-roles)))))

  (let [user-id (create-test-user)]
    (testing "Retrieves user"
      (let [found-user (service/find-user-by-id user-id)]
        (is (= "test@gmail.com" (found-user :email)))))

    (testing "Finds user by email"
      (let [found-user (service/find-user-by-email "test@gmail.com")]
        (is (= user-id (found-user :id)))))))
 
(deftest multiple-user-operations
  (testing "Finds all users"
    (doall (map create-test-user (range 10)))
    (is (= 10 (count (service/find-all-users))))))

(deftest update-users
  (testing "Modifies existing user"
    (let [user {:email "alice@gmail.com" :password "pass123"
                :user-roles [2]}
          id (service/add-user user)]
      (service/update-user (assoc user :id id :email "cody@gmail.com"))
      (is (= "cody@gmail.com" ((service/find-user-by-id id) :email))))))

(deftest delete-users
  (testing "Decreases user count"
    (let [id (create-test-user)
          count-orig (service/count-users)
          count-role-orig (service/count-user-roles)]
      (service/delete-user id)
      (is (= (dec count-orig) (service/count-users)))
      (is (= (- 2 count-role-orig) (service/count-user-roles)))))
  
  (testing "Deletes correct user"
    (let [id-keep (create-test-user "keep")
          id-del (create-test-user "del")]
      (service/delete-user id-del)
      (is (= id-keep (:id (service/find-user-by-id id-keep)))))))

(deftest authorize-users
  (let [user-id (create-test-user)]
    (testing "Accepts user with correct password"
      (let [[ok? results] (service/authenticate-user {:email "test@gmail.com"
                                           :password "pass123"})]
        (is ok?)))
    (testing "Does not accept user with incorrect password"
      (let [[ok? results] (service/authenticate-user {:email "test@gmail.com"
                                           :password "shallnotpass"})]
        (is (not ok?))))
    (let [refresh-token-count (service/count-refresh-tokens)]
      (testing "Creates proper authorization token"
        (let [[ok? results] (service/create-auth-token
                             {:private-key "wings_auth_privkey.pem"
                              :passphrase "l@s@gn@"}
                             {:email "test@gmail.com"
                              :password "pass123"})]
          (is (results :token-pair))
          (is (not (= user-id (-> results :token-pair
                                  :auth-token :user :public-id))))))
      (testing "Refresh token persisted"
        (is (= (inc refresh-token-count) (service/count-refresh-tokens)))))))

(deftest refresh-tokens      
  (let [user-id (create-test-user)
        auth-conf {:private-key "wings_auth_privkey.pem"
                   :public-key "wings_auth_pubkey.pem"
                   :passphrase "l@s@gn@"}
        [ok? results] (service/create-auth-token
                       auth-conf
                       {:email "test@gmail.com"
                        :password "pass123"})
        refresh-token (-> (:token-pair results) (:refresh-token))
        orig-token-count (service/count-refresh-tokens)]
    (testing "Refreshes auth token with good refresh token"
      (let [[ok? results] (service/refresh-auth-token auth-conf
                                                      refresh-token)]
        (is (:token-pair results) (:message results))))
    (testing "New refresh token created in db"
      (is (= (inc orig-token-count) (service/count-refresh-tokens))))
    (testing "Does not refresh auth token with shoddy refresh token"
      (let [[ok? results] (service/refresh-auth-token auth-conf
                                                      "thistokennogood")]
        (is (not ok?))))
    (testing "Does not refresh expired refresh token."
      (with-redefs  [time/now (fn [] (time/plus (time/now)
                                                (time/minutes 40)))] )
      (let [[ok? results] (service/refresh-auth-token auth-conf
                                                      refresh-token)]
        (is (not ok?))))))

  
