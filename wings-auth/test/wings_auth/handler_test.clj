(ns wings-auth.handler-test
  (:use clojure.test
        ring.mock.request
        wings-auth.core)
  (require [wings-auth.service :as service]
           [clojure.data.json :as json]))

(defn clean-database [f]
  (service/delete-all-users)
  (f))

(defn- create-test-user
  ([] (create-test-user ""))  
  ([identifier]
   (service/add-user {:email (str "test" identifier "@gmail.com")
                  :password "pass123" :user-roles [1, 2]})))

(use-fixtures :each clean-database)

(deftest main-routes
  (testing "create authorization token route"
    (create-test-user)
    (let [response (app  (-> (request :post "/create-auth-token" 
                                      (json/write-str {:email "test@gmail.com"
                                                       :password "pass123"}))
                             (content-type "application/json")))]
      (println (get-in response [:body]))
      (is (= (:status response) 201))
      (is (= (get-in response [:headers "Content-Type"])
             "application/json; charset=utf-8"))))
  
  (testing "not found route"
    (let [response (app (request :get "/this-fake"))]
      (is (= (:status response) 404)))))

(deftest signing-up
  (testing "POST /sign-up"
    (let [user-count (service/count-users)
          user-role-count (service/count-user-roles)
          response (app (-> (request :post "/sign-up")
                            (body (json/write-str {:email "test@gmail.com"
                                                   :password "pass123"}))
                            (content-type "application/json")
                            (header "Accept" "application/json")))]
      (is (= (:status response) 201))
      (is (= (inc user-count) (service/count-users)))
      (is (= (inc user-role-count) (service/count-user-roles)))))
  (testing "POST /sign-up user that already exists"
    (let [response (app (-> (request :post "/sign-up")
                            (body (json/write-str {:email "test@gmail.com"
                                                   :password "pass222"}))
                            (content-type "application/json")
                            (header "Accept" "application/json")))]
      (is (= (:status response) 404)))))
