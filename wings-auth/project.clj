(defproject wings-auth "0.1.0-SNAPSHOT"
  :description "Wings authentication service"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [ring/ring-core "1.4.0"]
                 [ring/ring-jetty-adapter "1.4.0"]
                 [compojure "1.4.0"]
                 [ring/ring-json "0.4.0"]
                 [org.mariadb.jdbc/mariadb-java-client "1.1.7"]
                 [ragtime "0.5.2"]
                 [yesql "0.5.1"]
                 [environ "1.0.1"]
                 [org.clojure/data.json "0.2.6"]
                 [buddy/buddy-sign "0.9.0"]
                 [buddy/buddy-core "0.9.0"]
                 [buddy/buddy-hashers "0.9.0"]
                 [jstrutz/hashids "1.0.1"]]
  :main ^:skip-aot wings-auth.core
  :plugins [[lein-ring "0.8.10"]]
  :aliases {"migrate"  ["run" "-m" "wings-auth.core/migrate"]
            "rollback" ["run" "-m" "wings-auth.core/rollback"]}
  :ring {:handler wings-auth.core/app
         :nrepl {:start? true
                 :port 9988}}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring-mock "0.1.5"]]}
   :test {:ragtime {:database
"jdbc:mariadb://localhost:3306/wings_auth_test?user=wings_test&password=pass_test"}}})

