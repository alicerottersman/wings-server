CREATE TABLE IF NOT EXISTS `locations` (
  `idlocations` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `lat` DECIMAL NOT NULL,
  `long` DECIMAL NOT NULL,
  `cross_street` VARCHAR(100) NULL,
  PRIMARY KEY (`idlocations`));
