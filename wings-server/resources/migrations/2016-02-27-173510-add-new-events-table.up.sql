CREATE TABLE IF NOT EXISTS `events` (
  `idevents` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `description` TEXT NULL,
  `date` BIGINT NULL,
  `organizer` INT NOT NULL,
  `location` INT NOT NULL,
  `gender` ENUM('male', 'female', 'other') NOT NULL,
  `sexual_orientation` ENUM('male', 'female', 'both', 'other') NOT NULL,
  `bio` VARCHAR(240) NULL,
  `age_start` INT NULL,
  `age_end` INT NULL,
  `type` VARCHAR(45) NOT NULL,
  `open` TINYINT NOT NULL,
   PRIMARY KEY (`idevents`),
  INDEX `organizer_idx`(`organizer` ASC),
  CONSTRAINT `organizer`
    FOREIGN KEY (`organizer`)
    REFERENCES `users`(`idusers`)
    ON DELETE CASCADE);
