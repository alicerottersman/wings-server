CREATE TABLE IF NOT EXISTS `event_participants` (
  `idevents` INT NULL,
  `idusers` INT NULL,
  INDEX `idevents_idx` (`idevents` ASC),
  INDEX `idusers_idx` (`idusers` ASC),
  CONSTRAINT `idevents`
    FOREIGN KEY (`idevents`)
    REFERENCES `events` (`idevents`)
    ON DELETE CASCADE,
  CONSTRAINT `idusers`
    FOREIGN KEY (`idusers`)
    REFERENCES `users` (`idusers`)
    ON DELETE CASCADE);
