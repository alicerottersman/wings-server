CREATE TABLE IF NOT EXISTS `events` (
  `idevents` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `description` TEXT NULL,
  `date` DATETIME NULL,
  `organizer` INT NOT NULL,
   PRIMARY KEY (`idevents`),
  INDEX `organizer_idx`(`organizer` ASC),
  CONSTRAINT `organizer`
    FOREIGN KEY (`organizer`)
    REFERENCES `users`(`idusers`)
    ON DELETE CASCADE);
