CREATE TABLE IF NOT EXISTS `message` (
  `idmessage` INT NOT NULL AUTO_INCREMENT,
  `timestamp` TIMESTAMP NOT NULL,
  `from_user_id` INT NOT NULL,
  `destination_type` ENUM('group', 'user') NOT NULL,
  `iddestination` INT NOT NULL,
  `message` BLOB NULL,
  PRIMARY KEY (`idmessage`),
  INDEX `fromuserid_idx` (`from_user_id` ASC),
  CONSTRAINT `from_user_id`
    FOREIGN KEY (`from_user_id`)
    REFERENCES `users` (`idusers`)
    ON DELETE CASCADE);
