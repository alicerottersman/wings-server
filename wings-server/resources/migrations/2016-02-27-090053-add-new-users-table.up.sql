CREATE TABLE IF NOT EXISTS `users` (
  `idusers` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `public_id` VARCHAR(45) NOT NULL,
  `gender` ENUM('male', 'female', 'other') NOT NULL,
  `sexual_orientation` ENUM('male', 'female', 'both', 'other') NOT NULL,
  `bio` VARCHAR(240) NULL,
  `dob` BIGINT NOT NULL,
  `default_location` INT NOT NULL,
  `image_url` VARCHAR(240) NULL,
  `image_thumb_url` VARCHAR(240) NULL,
  PRIMARY KEY (`idusers`),
  UNIQUE INDEX `public_id_UNIQUE` (`public_id` ASC));
