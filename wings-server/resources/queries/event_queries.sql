-- name: create-event<!
-- Creates an event
INSERT INTO `events` (
                     `title`,
                     `description`,
                     `date`,
                     `organizer`,
                     `location`,
                     `gender`,
                     `sexual_orientation`,
                     `age_start`,
                     `age_end`,
                     `type`,       
                     `open` )
VALUES (:title,
        :description,
        :date,        
        :organizer,
        :location,
        :gender,
        :sexual_orientation,
        :age_start,
        :age_end,
        :type,
        :open);

-- name: count-events
-- Counts all events
SELECT COUNT(*) as count FROM `events`;

-- name: delete-all-events!
-- Delete all events. For us with testing.
DELETE FROM `events`;

-- name: find-event-by-id
-- Get an event by its id
SELECT `events`.* FROM `events` WHERE idevents=:id

-- name: find-events-by-organizer-id
-- Finds all events listed under a given user
SELECT * FROM `events` WHERE `organizer`=:organizer;

-- name: find-events-after-date
-- Find all events that are planned to occur after the given date
SELECT * FROM `events` WHERE `date`>=:date;

-- name: find-events-in-range
-- Find all events planned for the given date range
SELECT * FROM `events` WHERE `date` BETWEEN :start AND :end;

-- name: find-events-by-organizer-id-and-date
-- Find all events planned by organizer after date
SELECT * FROM `events` WHERE `date`>=:date AND `organizer`=:organizer

-- name: find-all-events
-- Get all events
SELECT * FROM `events`;

-- name: delete-event!
-- Delete event with matching id
DELETE FROM `events` WHERE `idevents`=:id;

-- name: update-event!
-- Update event with matching id
UPDATE `events` SET `title`=:title,
                    `description`=:description,
                    `date`=:date,
                    `organizer`=:organizer,
                    `location`=:location,
                    `gender`=:gender,
                    `sexual_orientation`=:sexual_orientation,
                    `age_start`=:age_start,
                    `age_end`=:age_end,
                    `type`=:type,       
                    `open`=:open
WHERE `idevents`=:idevents;

-- name: find-organizer-for-event
-- Find the organizer of an event
SELECT `users`.* FROM `users`
INNER JOIN `events`
ON `events`.`organizer` = `users`.`idusers`
WHERE `events`.`idevents` = :event_id

-- name: find-events-by-organizer-public-id
-- Find all events under an organizer's public id
SELECT `events`.* FROM `events`
INNER JOIN `users`
ON `events`.`organizer` = `users`.`idusers`
WHERE `users`.`public_id`=:public_id

-- name: find-events-by-organizer-public-id-after-date
-- Find all events after a date under organizer's public id
SELECT `events`.* FROM `events`
INNER JOIN `users`
ON `events`.`organizer` = `users`.`idusers`
WHERE `users`.`public_id`=:public_id
AND `events`.`date` >= :date

-- name: find-filtered-events
-- Find all events under the search parameters
SELECT `events`.* FROM `events`
WHERE `title` = COALESCE(:title, `title`)
AND `description` LIKE COALESCE (:description, `description`)
AND `date` <= COALESCE(:date_end, `date`)
AND `date` >= COALESCE(:date_start, `date`)
AND `gender` = COALESCE(:gender, `gender`)
AND `sexual_orientation` = COALESCE(:sexual_orientation, `sexual_orientation`)
AND `age_start` <= COALESCE(:age_end, `age_start`)
AND `age_end` >= COALESCE(:age_start, `age_end`)
AND `type` = COALESCE(:type, `type`)
AND `open` = COALESCE(:open, `open`);

