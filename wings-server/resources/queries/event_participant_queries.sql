-- name: create-participant!
-- Creates a participant with a user id for the specified list
INSERT INTO `event_participants` (`idusers`, `idevents`)
VALUES (:user_id, :event_id);

-- name: delete-all-participants!
-- Delete all participants for all events
DELETE FROM `event_participants`;

-- name: delete-participant!
-- Delete participant from event
DELETE FROM `event_participants`
WHERE `idevents`=:event_id AND `idusers`=:user_id

-- name: find-participants-for-event
-- Find all the participants for the given event id
SELECT `users`.* FROM `users`
INNER JOIN `event_participants`
ON `users`.`idusers` = `event_participants`.`idusers`
WHERE `event_participants`.`idevents`=:event_id

-- name: find-participant-list-view-for-event
-- Find all the participants for the given event id
SELECT `users`.`name`, `users`.`dob`,
`users`.`image_thumb_url`, `users`.`public_id`
FROM `users`
INNER JOIN `event_participants`
ON `users`.`idusers` = `event_participants`.`idusers`
WHERE `event_participants`.`idevents`=:event_id LIMIT :limit

-- name: find-events-for-participant
-- Find all events a user is a participant of
SELECT `events`.* FROM `events`
INNER JOIN `event_participants`
ON `events`.`idevents` = `event_participants`.`idevents`
WHERE `event_participants`.`idusers`=:user_id

-- name: find-events-for-participant-public-id
-- Find all events a user with the pub id is a participant of
SELECT `events`.* FROM `events`
LEFT JOIN `event_participants`
     ON `events`.`idevents` = `event_participants`.`idevents`
LEFT JOIN `users`
     ON `users`.`idusers` = `event_participants`.`idusers`
WHERE `users`.`public_id`=:public_id

-- name: find-events-for-participant-public-id-after-date
-- Find all events for a user with pub id after a date
SELECT `events`.* FROM `events`
LEFT JOIN `event_participants`
     ON `events`.`idevents` = `event_participants`.`idevents`
LEFT JOIN `users`
     ON `users`.`idusers` = `event_participants`.`idusers`
WHERE `users`.`public_id`=:public_id
AND `events`.`date` >= :date;

-- name: count-participants-for-event
-- Counts all the participants for the given event
SELECT COUNT(*) as count FROM `event_participants`
WHERE idevents=:event_id;

-- name: find-events-for-participant-after-date
-- Finds all the events a user will participate in
-- after a date
SELECT `events`.* FROM `events`
INNER JOIN `event_participants`
ON `events`.`idevents`=`event_participants`.`idevents`
WHERE `event_participants`.`idusers`=:user_id 
AND `events`.`date` >= :date
