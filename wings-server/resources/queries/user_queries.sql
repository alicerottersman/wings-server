-- name: create-user<!
-- Creates a user with the given profile info (name, gender, public-id)
INSERT INTO `users` (`name`,
                     `gender`,
                     `public_id`,
                     `sexual_orientation`,
                     `bio`,
                     `dob`)
VALUES (:name, :gender, :public_id, :sexual_orientation, :bio, :dob);

-- name: count-users
-- Counts all users
SELECT COUNT(*) as count FROM `users`;

-- name: delete-all-users!
-- Delete all users. For use with testing.
DELETE FROM `users`;

-- name: find-user-by-id
-- Get user by id
SELECT * FROM `users` WHERE idusers=:id

-- name: find-user-by-public-id
-- Get user by public-id
SELECT * FROM `users` WHERE `public_id`=:public_id

-- name: find-all-users
-- Get all users
SELECT * FROM `users`;

-- name: update-user!
-- Update the user with matching public  id
UPDATE `users` SET `name`=:name,
                   `gender`=:gender,
                   `sexual_orientation`=:sexual_orientation,
                   `bio`=:bio,
                   `dob`=:dob
WHERE  `public_id`=:public_id;

-- name: delete-user!
-- Delete user with matching id
DELETE FROM `users` WHERE `idusers`=:id;

-- name: delete-user-with-public-id!
-- Delete user with matching public-id
DELETE FROM `users` WHERE `public_id`=:public_id;

