-- name: create-location<!
-- Create a new location and return its id
INSERT INTO `locations` 
(`name`, `lat`, `long`, `cross_street`)
VALUES (:name, :lat, :long, :cross_street);

-- name: find-location-by-id 
-- Find a location by its id
SELECT
`locations`.`name`,
`locations`.`lat`,
`locations`.`long`,
`locations`.`cross_street`
FROM `locations` WHERE idlocations = :id

-- name: delete-all-locations!
-- Delete all the locations stored
DELETE FROM `locations`
