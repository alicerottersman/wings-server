(ns wings-server.handler-test
  (:use clojure.test
        wings-server.core-test
        ring.mock.request
        wings-server.handler.core
        wings-server.test-data)
  (:require [wings-server.models.users :as users]))

(use-fixtures :each clean-database)

(deftest main-routes
  (testing "users endpoint"
    (let [response (app (-> (request :get "/users")
                            (header "Authorization" (auth-token test-role-admin
                                                                "TEST"))))]
      (is (= (:status response) 200))
      (is (= (get-in response [:headers "Content-Type"]) "application/json; charset=utf-8"))))                    

  (testing "events endpoint"
    (let [response (app (-> (request :get "/events")
                            (header "Authorization" (auth-token test-role-admin
                                                                "TEST"))))]
      (is (= (:status response) 200))
      (is (= (get-in response [:headers "Content-Type"]) "application/json; charset=utf-8"))))

  (testing "not found route"
    (let [response (app (request :get "/this-fake"))]
      (is (= (:status response) 404)))))
