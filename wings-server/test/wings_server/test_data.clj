(ns wings-server.test-data
  (:require  [buddy.core.keys :as auth-keys]
             [clj-time.core :as time]
             [clojure.java.io :as io]
             [buddy.sign.util :as util]
             [buddy.sign.jws :as jws]
             [wings-server.models.users :as users]
             [wings-server.models.events :as events]))

(def test-name "Alice Rottersman")
(def test-gender "female")
(def test-so "male")
(def test-bio "I code this sh--")
(def test-dob (-> (time/date-time 1994 04 25)
                  (util/to-timestamp)))
(def test-public-id-base "TEST")

(def test-user {:name test-name
                 :gender test-gender
                 :sexual_orientation test-so
                 :bio test-bio
                 :dob test-dob
                 :image_url nil
                 :image_thumb_url nil})

(def test-email "test@gmail.com")
(def test-role-user [2])
(def test-role-admin [1,2])

(def test-event-title "Ballers Unite")
(def test-event-description
  "A meeting of some dope ballers. Hang out, pickup some fine ladies.")
(def test-event-date  (time/plus (time/now) (time/days 7)))
(def test-event-timestamp (util/to-timestamp test-event-date))
(def test-event-gender "female")
(def test-event-so "any")
(def test-event-type "bar crawl")
(def test-event-open 0)
(def test-event-age-start 21)
(def test-event-age-end 26)

(def test-location {:name "The Burren"
                    :lat 41.44
                    :long 54.22
                    :cross_street "5th ave and 31st"})

(def test-event {:title test-event-title
                 :description test-event-description
                 :gender test-event-gender
                 :sexual_orientation test-event-so
                 :age_start test-event-age-start
                 :age_end test-event-age-end
                 :type test-event-type
                 :open test-event-open
                 :location test-location})

(defn make-test-token [user]
  (let [expiration (-> (time/plus (time/now) (time/minutes 30))
                       (util/to-timestamp))]
    (jws/sign {:user user}
              (auth-keys/private-key
               (io/resource "wings_auth_privkey.pem")
               "l@s@gn@")
              {:alg :rs256 :exp expiration})))

(defn auth-token
  ([] (auth-token test-role-user test-public-id-base))
  ([role public-id] (str "Token " (make-test-token
                              {:email test-email
                               :user-roles role
                               :public-id public-id}))))

(defn create-test-user
  ([] (create-test-user ""))  
  ([identifier]
   (users/create (assoc test-user
                   :public_id (str test-public-id-base identifier)))))

(defn create-test-event
  ([user-id]
   (create-test-event user-id 0))
   
  ([user-id day-offset]
   (events/create (assoc  test-event
                    :date (util/to-timestamp
                           (time/plus
                            test-event-date (time/days day-offset)))
                    :organizer user-id))))
