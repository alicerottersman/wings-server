(ns wings-server.event-handler-test
  (:use clojure.test
        clojure.data
        wings-server.core-test
        ring.mock.request
        wings-server.handler.core
        wings-server.test-data)
  (:require [wings-server.models.events :as events]
            [cheshire.core :as json]))

(use-fixtures :each clean-database)

(deftest creating-event
  (testing "POST /events"
    (let [event-count (events/count-events)
          user-id (create-test-user)
          response (app (-> (request :post "/events")
                            (body (json/generate-string
                                   (assoc (dissoc test-event :organizer)
                                     :organizer-public-id test-public-id-base
                                     :date test-event-timestamp)))
                            (content-type "application/json")
                            (header "Accept" "application/json")
                            (header "Authorization" (auth-token))))]
      (is (= 201 (response :status)) response)
      (is (= (inc event-count) (events/count-events)))
      (is (substring? "/events" (get-in response [:headers "Location"]))))))

(deftest fetching-event-info
  (testing "GET /events"
    (create-test-event (create-test-user))
    (create-test-event (create-test-user "2"))
    (create-test-event (create-test-user "3"))
    (let [event-count (events/count-events)
          response (app (-> (request :get "/events")
                            (header "Authorization"
                                    (auth-token test-role-admin
                                                test-public-id-base))))
          response-data (json/parse-string (:body response))]
      (is (= 200 (response :status)))
      (is (substring? test-event-title (response :body)))
      (is (= event-count (count (get response-data "results" []))))
      (is (= event-count (get response-data "count" [])))

      (testing "GET /events?"
        (let [response (app (-> (request :get
                                         "/events?gender=female&age_start=19")
                                (header "Authorization" (auth-token))))
              response-data (json/parse-string (:body response))]
          (is (= 200 (response :status)))
          (is (substring? test-event-title (response :body)))
          (is (= event-count (count (get response-data "results" []))))
          (is (= event-count (get response-data "count")))))))
  
  (let [public-id (str test-public-id-base 100)
        user-id (create-test-user 100)
        event-id (create-test-event user-id)
        desired-response (assoc test-event
                           :organizer
                           {:public_id public-id
                            :name test-name
                            :image_thumb_url nil
                            :dob test-dob}
                           :date test-event-timestamp
                           :idevents event-id
                           :participants [])
        response (app (-> (request :get (str "/events/" event-id))
                          (header "Authorization" (auth-token))))]
    (testing "GET /events/<id> with 0  participants"
      (let [different? (diff (json/parse-string (response :body) true)
                             desired-response)]
        (is (= different? [nil nil desired-response])
            (str "Different because ... "  (first different?)
                 " and " (second different?)))))
    
    (testing "GET /events/<id> with participants"
      (let [participant-id-1 (create-test-user "p1")
            participant-id-2 (create-test-user "p2")]
        (events/add-participant event-id participant-id-1)
        (events/add-participant event-id participant-id-2)
        (let [response (app (-> (request :get (str "/events/" event-id))
                                (header "Authorization" (auth-token))))
              parsed-response (json/parse-string (response :body) true)
              different? (diff parsed-response
                               (assoc desired-response :participants
                                      [{:public_id
                                        (str test-public-id-base "p1")
                                        :name test-name
                                        :image_thumb_url nil
                                        :dob test-dob }
                                       {:public_id
                                        (str test-public-id-base "p2")
                                        :name test-name
                                        :image_thumb_url nil
                                        :dob test-dob}]))]
          (is (= different? [nil nil parsed-response])
               (str "Different because ... "  (first different?)
                 " and " (second different?))))))))


(deftest updating-event
  (testing "POST /events/<public-id>"
    (let [event-id (create-test-event (create-test-user))
          response (app (-> (request :post (str "/events/" event-id))
                            (body (json/generate-string
                                   (assoc (dissoc test-event :organizer)
                                     :idevents event-id
                                     :organizer-public-id test-public-id-base
                                     :date test-event-timestamp
                                     :description "JK we nerds")))
                            (content-type "application/json")
                            (header "Accept" "application/json")
                            (header "Authorization" (auth-token))))]
      (is (= 201 (response :status)) response)
      (is (= "JK we nerds" (:description (events/find-by-id event-id))))
      (is (substring? "/events/" (get-in response [:headers "Location"]))))))

(deftest deleting-event
  (testing "DELETE /events/<public-id>"
    (let [event-id (create-test-event (create-test-user))
          event-count (events/count-events)
          response (app (-> (request :delete
                                     (str "/events/" event-id))
                            (header "Authorization" (auth-token))))]
      (is (= 200 (response :status)) response)
      (is (= (dec event-count) (events/count-events))))))
