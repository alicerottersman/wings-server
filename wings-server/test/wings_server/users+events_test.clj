(ns wings-server.users+events-test
  (:use clojure.test
        wings-server.core-test
        wings-server.test-data)
  (:require [wings-server.models.users :as users]
            [wings-server.models.events :as events]))

(use-fixtures :each clean-database)

(deftest event-for-user-operations
  (let [user-id (create-test-user)
        event-id (create-test-event user-id)
        participant-id (create-test-user 1)]

    (testing "Retrieves all events organized under user id"
      (let [found-event (first (events/find-by-organizer-id user-id))]
      (is (= user-id (found-event :organizer)))))

    (testing "Retrieves all events organized searched by public id"
      (let [found-event (first (events/find-by-organizer-public-id
                                test-public-id-base))]
        (is (= user-id (found-event :organizer)))))

    (testing "Retrieves all current events a user is organizing searched
by public id."
      (let [old-event-id (create-test-event user-id -20)
            found-events (events/find-current-by-organizer-public-id
                          test-public-id-base)
            current-events (events/find-current-events)]
        (is (= current-events found-events))))
    
    (testing "Retrieves all events a user is participating in"
      (events/add-participant event-id participant-id)
      (let [found-events (events/find-by-participant-id participant-id)
            event (events/find-by-id event-id)]
        (is (= [event] found-events))))

    (testing "Retrieves all events a user is participating in searched
by public id"
      (is (= (events/find-by-participant-id participant-id)
             (events/find-by-participant-public-id
              (str test-public-id-base 1)))))
    
    (testing "Retrieves all current events a user is participating in"
      (let [old-event-id (create-test-event user-id -20)]
        (events/add-participant old-event-id participant-id))
      (let [found-events (events/find-current-by-participant-id participant-id)
            current-events (events/find-current-events)]
        (is (= current-events found-events))))))

(deftest user-for-event-operations
  (let [user-id (create-test-user)
        event-id (create-test-event user-id)
        participant-id (create-test-user 1)]
    (events/add-participant event-id participant-id)
    (testing "Retrieves participants of event"
      (is (= [(users/find-by-id participant-id)]
             (events/find-participants event-id))))
    
    (testing "Retrieves organizer of event"
      (is (= (users/find-by-id user-id)
             (events/find-organizer event-id))))))

(deftest cascade-operations
  (testing "When user is deleted, so are events they created"
    (let [user-id (create-test-user)
          event-id (create-test-event user-id)
          count-orig (events/count-events)]
      (users/delete-user user-id)
      (is (= (dec count-orig) (events/count-events))))))



