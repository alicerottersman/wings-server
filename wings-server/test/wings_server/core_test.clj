(ns wings-server.core-test
  (use wings-server.test-data)
  (:require [clojure.test :refer :all]
            [wings-server.core :refer :all]
            [wings-server.models.users :as users]
            [wings-server.models.events :as events]))

(defn clean-database [f]
  (users/delete-all)
  (events/delete-all)
  (f))

(defn substring? [sub st]
  (if (nil? st)
    false
    (not= (.indexOf st sub) -1)))


