(ns wings-server.users-test
  (:use clojure.test
        wings-server.core-test
        wings-server.test-data)
  (:require [wings-server.models.users :as users]))


(use-fixtures :each clean-database)

(deftest create-read-users
  (testing "Creates user"
    (let [count-orig (users/count-users)]
      (create-test-user 0)
      (is (= (inc count-orig) (users/count-users)))))

  (let [user-id (create-test-user)]
    (testing "Retrieves user"
      (let [found-user (users/find-by-id user-id)]
        (is (= test-name (found-user :name)))))

    (testing "Retrieves user by public id"
      (let [found-user (users/find-by-public-id test-public-id-base)]
        (is (= user-id (found-user :idusers)))
        (is (= test-so (found-user :sexual_orientation)))))))
 
(deftest multiple-user-operations
  (testing "Finds all users"
    (doall (map create-test-user (range 10)))
    (is (= 10 (count (users/find-all))))))

(deftest update-users
  (testing "Modifies existing user"
    (let [id (create-test-user)
          user (users/find-by-id id)]
      (users/update-user (assoc user :idusers id :name "Cody Brittain"))
      (is (= "Cody Brittain" ((users/find-by-id id) :name))))))

(deftest delete-users
  (testing "Decreases user count"
    (let [id (create-test-user)
          count-orig (users/count-users)]
      (users/delete-user id)
      (is (= (dec count-orig) (users/count-users)))))
  
  (testing "Deletes correct user"
    (let [id-keep (create-test-user "keep")
          id-del (create-test-user "del")]
      (users/delete-user id-del)
      (is (= id-keep (:idusers (users/find-by-id id-keep)))))))
