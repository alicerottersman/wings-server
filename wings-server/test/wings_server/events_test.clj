(ns wings-server.events-test
  (:use clojure.test
        wings-server.core-test
        wings-server.test-data)
  (:require [wings-server.models.events :as events]
            [wings-server.models.users :as users]
            [clj-time.core :as time]
            [clj-time.coerce :as coerce]))

(use-fixtures :each clean-database)

(defn- to-timestamp [date]
  (quot (coerce/to-long date) 1000))

(deftest create-read-events
  (let [user-id (create-test-user)]
    (testing "Creates event"
      (let [count-orig (events/count-events)]
        (create-test-event user-id)
        (is (= (inc count-orig) (events/count-events)))))

    (let [event-id (create-test-event user-id)]
      (testing "Retrieves event"
        (let [found-event (events/find-by-id event-id)]
          (is (= test-event-title (found-event :title)))
          (is (= user-id (found-event :organizer))))))))

(deftest multiple-events-operations
  (testing "Finds all events"
    (doall (map create-test-event
                (doall (map create-test-user (range 10)))
                (range -10 0)))
    (is (= 10 (count (events/find-all)))))

  (testing "Find current female events"
    (let [current-events (events/find-filtered-events
                          {:date_start (to-timestamp (time/now))
                           :gender test-gender})]
      (is (= 6 (count current-events)))))

  (testing "Find male events"
    (let [male-events (events/find-filtered-events {:gender "male"})]
      (is (= 0 (count male-events)))))

  (testing "Find events in date range for two ages"
    (let [start (time/plus (time/now) (time/days -4))
          end (time/plus (time/now) (time/days -3))
          filtered-events (events/find-filtered-events
                          {:date_start (to-timestamp start)
                           :date_end (to-timestamp end)
                           :age_start 19
                           :age_end 21})]
      (is (= 1 (count filtered-events))))))

(deftest update-event
  (testing "Modifies existing event"
    (let [event-id (create-test-event (create-test-user))
          event (events/find-by-id event-id)]
      (events/update-event (assoc event :title "Different title"))
      (is (= "Different title" ((events/find-by-id event-id) :title))))))

(deftest delete-event
  (testing "Decreases event count"
    (let [id (create-test-event (create-test-user))
          count-orig (events/count-events)]
      (events/delete-event id)
      (is (= (dec count-orig) (events/count-events)))))

  (testing "Deletes correct user"
    (let [id-del (create-test-event (create-test-user "del"))
          id-keep (create-test-event (create-test-user "keep"))]
      (events/delete-event id-del)
      (is (= id-keep (:idevents (events/find-by-id id-keep)))))))


(deftest add-remove-participant
  (let [participant-id1 (create-test-user "partic1")
        participant-id2 (create-test-user "partic2")
        event-id (create-test-event (create-test-user "org"))]
    
      (testing "Adds participant to event"
        (events/add-participant event-id participant-id1)
        (events/add-participant event-id participant-id2)
        (is (= [(users/find-by-id participant-id1)
                (users/find-by-id participant-id2)]
               (events/find-participants event-id))))
      
      (let [count-orig (events/count-event-participants event-id)]
        (testing "Removing participant decreases number of participants"
    
          (events/remove-participant event-id participant-id1)
          (is (= (dec count-orig) (events/count-event-participants event-id))))
          
        (testing "Removing participant does not delete user from db"
          (is (= participant-id1
                 (:idusers (users/find-by-id participant-id1))))))
      (testing "When event deleted, so are its participants"
        (events/delete-event event-id)
        (is (= 0 (events/count-event-participants event-id))))))


