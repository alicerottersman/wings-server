(ns wings-server.user-handler-test
  (:use clojure.test
        wings-server.core-test
        ring.mock.request
        wings-server.handler.core
        wings-server.test-data)
  (:require [wings-server.models.users :as users]
            [cheshire.core :as json]))

(use-fixtures :each clean-database)

(deftest creating-user
  (testing "POST /users"
    (let [user-count (users/count-users)
          response (app (-> (request :post "/users")
                            (body (json/generate-string (assoc test-user
                                                          :public-id "TEST")))
                            (content-type "application/json")
                            (header "Accept" "application/json")
                            (header "Authorization" (auth-token))))]
      (is (= 201 (response :status)) response)
      (is (= (inc user-count) (users/count-users)))
      (is (substring? "/users" (get-in response [:headers "Location"]))))))

(deftest fetching-user-info
  (testing "GET /users"
    (doall (map create-test-user (range 5)))
    (let [user-count (users/count-users)
          response (app (-> (request :get "/users")
                            (header "Authorization" (auth-token test-role-admin
                                                                test-public-id-base))))
          response-data (json/parse-string (:body response))]
      (is (= 200 (response :status)))
      (is (substring? test-name (response :body)))
      (is (= user-count (count (get response-data "results" []))))
      (is (= user-count (get response-data "count" [])))))

  (testing "GET /users/<public-id>"
    (let [user-id (create-test-user 100)
          public-id (str test-public-id-base 100)
          response (app (-> (request :get (str "/users/" public-id))
                            (header "Authorization" (auth-token test-role-user
                                                                public-id))))]
      (is (= (json/parse-string (response :body) true)
                (assoc test-user
                  :public_id public-id
                  :idusers user-id))))))

(deftest updating-user
  (testing "POST /users/<public-id>"
    (let [user-id (create-test-user)
          response (app (-> (request :post (str "/users/" test-public-id-base))
                            (body (json/generate-string
                                   (assoc test-user
                                                :bio "I don't play.")))
                            (content-type "application/json")
                            (header "Accept" "application/json")
                            (header "Authorization" (auth-token))))]
      (is (= 201 (response :status)) response)
      (is (= "I don't play." (:bio (users/find-by-id user-id))))
      (is (substring? "/users/" (get-in response [:headers "Location"]))))))

(deftest deleting-user
  (testing "DELETE /users/<public-id>"
    (let [user-id (create-test-user)
          user-count (users/count-users)
          response (app (-> (request :delete
                                       (str "/users/" test-public-id-base))
                              (header "Authorization" (auth-token))))]
      (is (= 200 (response :status)) response)
      (is (= (dec user-count) (users/count-users))))))
