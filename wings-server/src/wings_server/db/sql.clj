(ns wings-server.db.sql
  (:require [yesql.core :refer [defqueries]]
            [wings-server.db.specs :as db]))

(defqueries "queries/user_queries.sql" {:connection db/mariadb-spec})
(defqueries "queries/event_queries.sql" {:connection db/mariadb-spec})
(defqueries "queries/event_participant_queries.sql" {:connection
                                                     db/mariadb-spec})
(defqueries "queries/event+user_queries.sql" {:connection db/mariadb-spec})
(defqueries "queries/location_queries.sql" {:connection db/mariadb-spec})
