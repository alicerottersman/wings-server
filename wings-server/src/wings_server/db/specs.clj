(ns wings-server.db.specs)
(def mariadb-spec 
  {:connection-uri (System/getenv "WINGS_DB_URL")})
