(ns wings-server.core
  (:require [ragtime.jdbc :as jdbc]
            [ragtime.repl :as repl]
            [environ.core :refer [env]]))

(defn load-config []
  {:datastore  (jdbc/sql-database {:connection-uri  (env :wings-db-url)
                                   :migrations_table "ragtime_migrations"})
   :migrations (jdbc/load-resources "migrations")})
;zero argument functions for use with lein
(defn migrate []
  (repl/migrate (load-config)))

(defn rollback []
  (repl/rollback (load-config)))
