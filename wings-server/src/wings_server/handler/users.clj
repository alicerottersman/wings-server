(ns wings-server.handler.users
  (:require [wings-server.models.users :as users]
            [ring.util.response :refer [response]]))

(defn get-users [_]
  {:status 200
   :body {:count (users/count-users)
          :results (users/find-all)}})

(defn- format-user [user]
  (dissoc (assoc user :public_id (user :public-id)) :public-id))

(defn create-user [{user :body}]
  (users/create (format-user user))
  {:status 201
   :headers {"Location" "/users"}})  

(defn edit-user [public-id user]
  (users/update-user (assoc user :public_id public-id))
  {:status 201
   :headers {"Location" (str "/users/" public-id)}})  

(defn find-user [public-id]
  (response (users/find-by-public-id public-id)))

(defn delete-user[public-id]
  (response (users/delete-user-with-public-id public-id)))

