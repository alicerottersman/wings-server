(ns wings-server.handler.security
  (require [buddy.auth :refer (authenticated?)]
           [buddy.auth.accessrules :refer [success error]]
           [buddy.auth.backends.token :refer [jws-backend]]
           [buddy.core.keys :as auth-keys]
           [clojure.java.io :as io]
           [wings-server.models.events :as events]))

(def auth-backend (jws-backend {:secret (auth-keys/public-key
                                         (io/resource "wings_auth_pubkey.pem"))
                                :options {:alg :rs256}}))

(def wings-server-roles {:admin 1 :user 2})

 (defn any-granted? [request roles]                                       
   (seq
    (clojure.set/intersection
     (set (-> request :identity :user :user-roles))
     (set (vals (select-keys wings-server-roles roles))))))

(defn unauthorized-handler [req msg]
  {:status 401
   :body {:status :error
          :message (or msg "User not authorized")}})


(defn authenticated-user
  "Return a handler that determines if user authenticated"
  [request]
  (if (authenticated? request)
    true
    (error "User must be authenticated.")))

(defn user-authorized 
  "Return a handler that determines if the user is
  is authorized given the required role to access
  and the user's role in the request"
  [roles]
  (fn [request]
    (if (any-granted? request roles)
      (success)
      (error "User not authorized for that request."))))

(defn user-has-id
  "Return a handler that determines whether the
  authenticated user has a given ID"
  [has-id?]
  (fn [req]
    (if (has-id? req) 
      (success)
      (error (str "User does not have id given")))))

(defn same-id-in-body?
  "Returns whether or not the user has the same id as is in the body
  under the given key name. For use with a handler to get request object."
  [id-key]
  (fn [req]
    (= (get-in req [:body id-key])
       (get-in req [:identity :user :public-id]))))

(defn same-id?
  "Returns whether or not the user has the same id as is 
  the one passed in. For use with a handler to get request object."
  [id]
  (fn [req]
    (= id (get-in req [:identity :user :public-id]))))

