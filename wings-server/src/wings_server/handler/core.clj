(ns wings-server.handler.core
  (:use compojure.core
        ring.middleware.json
        wings-server.handler.security
        wings-server.handler.users
        wings-server.handler.events)
  (:require [compojure.handler :as handler]
            [ring.util.response :refer [response]]
            [compojure.route :as route]
            [wings-server.models.users :as users]
            [wings-server.models.events :as events]
            [buddy.auth.accessrules :refer [restrict]]
            [buddy.auth.middleware :refer [wrap-authentication
                                           wrap-authorization]]
            [wings-server.models.events :as events]))

(defn wrap-log-request [handler]
  "prints request"
  (fn [request]
    (println (str "REQUEST::\t" request))
    (handler request)))

(defroutes app-routes
  (context "/users" []
           (GET "/" [] (-> get-users
                           (restrict
                            {:handler {:and [(user-authorized [:admin])
                                             authenticated-user]}
                             :on-error unauthorized-handler})))
           (POST "/" [] (-> create-user 
                            (restrict
                             {:handler {:and [{:or
                                               [(user-has-id
                                                 (same-id-in-body?
                                                  :public-id))
                                                (user-authorized
                                                 [:admin])]}
                                              authenticated-user]}
                              :on-error unauthorized-handler})))
           (context "/:public-id" [public-id]
                    (GET "/" [] (find-user public-id))
                    (restrict
                     (routes
                      (POST "/" {user :body}
                            (edit-user public-id user))
                      (DELETE "/" [] (delete-user public-id)))
                     {:handler {:and [{:or [(user-has-id
                                             (same-id? public-id))
                                             (user-authorized [:admin])]}
                                      authenticated-user]}
                      :on-error unauthorized-handler})))
  
  (context "/events" []
           (GET "/" [] (-> get-events
                           (restrict
                            {:handler {:or [(user-authorized [:admin])
                                             authenticated-user]}
                             :on-error unauthorized-handler})))
           (POST "/" [] (-> create-event
                            (restrict
                             {:handler {:and [authenticated-user
                                              {:or
                                               [(user-has-id
                                                 (same-id-in-body?
                                                  :organizer-public-id))
                                                (user-authorized
                                                 [:admin])]}]}
                              :on-error unauthorized-handler})))
           (context "/:event-id" [event-id]
                    (GET "/" [] (find-event event-id))
                    (let [organizer-id
                          (:public_id (events/find-organizer event-id))]
                      (restrict
                       (routes
                        (POST "/" {event :body}
                              (edit-event event))
                        (DELETE "/" [] (delete-event event-id)))
                       {:handler {:and [authenticated-user
                                        {:or
                                         [(user-has-id
                                           (same-id?
                                            organizer-id))
                                          (user-authorized [:admin])]}]}
                        :on-error unauthorized-handler}))))
  
  (route/not-found
   (response {:message "Page not found. Sorry!"})))

(def app
  (-> app-routes
; TODO wrap-log-request should be used in dev mode only.
      (wrap-authentication auth-backend)
      (wrap-authorization auth-backend)
      wrap-log-request
      wrap-json-response
      (wrap-json-body {:keywords? true}))) 
  
