(ns wings-server.handler.events
  (:require [wings-server.models.events :as events]
            [wings-server.models.users :as users]
            [wings-server.models.locations :as locations]
            [ring.util.response :refer [response]]))

(defn get-events
  ([]
   {:status 200
    :body {:count (events/count-events)
           :results (events/find-all)}})
  ([filter-keys]
   (let [found-events (events/find-filtered-events filter-keys)]
     {:status 200
      :body {:count (count found-events)
             :results found-events}})))

(defn- format-event [event]
  (let [organizer-id  (:idusers
                       (users/find-by-public-id
                        (:organizer-public-id event)))]
    (assoc (dissoc event :organizer-public-id) :organizer organizer-id)))

(defn create-event [{event :body}]
  (events/create (format-event event))
  {:status 201
   :headers {"Location" "/events"}})

(defn edit-event [event]
   (events/update-event (format-event event))
  {:status 201
   :headers {"Location" (str "/events/" (event :idevents))}})

(defn find-event [event-id]
  (let [event (events/find-by-id event-id)
        organizer (users/find-by-id (event :organizer))
        location (dissoc (locations/find-by-id (event :location))
                         :idlocations)]
    (response (assoc event
                :organizer {:name (organizer :name)
                            :dob (organizer :dob)
                            :image_thumb_url (organizer :image_thumb_url)
                            :public_id (organizer :public_id)}
                :location location
                :participants (events/find-participant-list event-id)))))

(defn delete-event [event-id]
  (response (events/delete-event event-id)))
