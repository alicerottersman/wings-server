(ns wings-server.models.events
  (:require [wings-server.db.sql :as sql]
            [clj-time.core :as time]
            [clj-time.coerce :as coerce]
            [wings-server.models.locations :as locations]))

(defn create [event]
  (let [location (event :location)
        location-id (locations/create location)]
    (:insert_id (sql/create-event<! (assoc event :location location-id)))))

(defn count-events []
  ((first (sql/count-events)) :count))

(defn find-by-id [id]
  (first (sql/find-event-by-id {:id id})))

(defn find-by-organizer-id [user-id]
  (sql/find-events-by-organizer-id {:organizer user-id}))

(defn find-by-organizer-public-id [public-id]
  (sql/find-events-by-organizer-public-id {:public_id public-id}))

(defn find-all []
  (sql/find-all-events))

(defn- to-timestamp [date]
  (quot (coerce/to-long date) 1000))

(defn find-current-events []
  (sql/find-events-after-date {:date (to-timestamp (time/now))}))

(defn find-current-by-organizer-id [user-id]
  (sql/find-events-by-organizer-id-and-date {:organizer user-id
                                             :date (to-timestamp (time/now))}))

(defn find-current-by-organizer-public-id [user-id]
  (sql/find-events-by-organizer-public-id-after-date {:public_id user-id
                                                      :date (to-timestamp
                                                       (time/now))}))

(defn find-events-after-date [timestamp]
  (sql/find-events-after-date {:date timestamp}))

(defn find-events-in-date-range [start-timestamp end-timestamp]
  (sql/find-events-in-range {:start start-timestamp
                             :end end-timestamp}))

(defn find-organizer [event-id]
  (first (sql/find-organizer-for-event {:event_id event-id})))


(defn update-event [event]
  (sql/update-event! event))


(defn add-participant [event-id user-id]
  (sql/create-participant! {:user_id user-id
                            :event_id event-id}))

(defn remove-participant [event-id user-id]
  (sql/delete-participant! {:user_id user-id
                            :event_id event-id}))

(defn find-participants [event-id]
  (sql/find-participants-for-event {:event_id event-id}))

(defn find-participant-list
  ([event-id]
   (find-participant-list event-id 1000))
  
  ([event-id limit]
   (sql/find-participant-list-view-for-event {:event_id event-id
                                              :limit limit})))

(defn find-by-participant-id [user-id]
  (sql/find-events-for-participant {:user_id user-id}))

(defn find-by-participant-public-id [public-id]
  (sql/find-events-for-participant-public-id {:public_id public-id}))

(defn find-current-by-participant-id [user-id]
  (sql/find-events-for-participant-after-date {:user_id user-id
                                               :date (to-timestamp (time/now))}))

(defn find-current-by-participant-public-id [public-id]
  (sql/find-events-for-participant-public-id-after-date {:public_id public-id
                                                         :date (to-timestamp
                                                                (time/now))}))

(defn merge-missing-keys
  [input complete]
  (merge-with #(or %1 %2) input  
              (into {} (map (fn[x] {x nil}) complete))))

(defn find-filtered-events [filter-keys]
  (let [all-keys [:title
                  :description
                  :date_end
                  :date_start
                  :gender
                  :sexual_orientation
                  :age_start
                  :age_end
                  :type
                  :open]
        merged-keys (merge-missing-keys filter-keys all-keys)]
    (sql/find-filtered-events merged-keys)))


(defn count-event-participants [event-id]
  (:count (first (sql/count-participants-for-event {:event_id event-id}))))

(defn delete-event [id]
  (sql/delete-event! {:id id}))

(defn delete-all []
  (sql/delete-all-events!))



