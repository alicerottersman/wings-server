(ns wings-server.models.locations
  (:require [wings-server.db.sql :as sql]))

(defn create [location]
  ((sql/create-location<! location) :insert_id))

(defn find-by-id [id]
  (first (sql/find-location-by-id {:id id})))

(defn delete-all []
  (sql/delete-all-locations!))
