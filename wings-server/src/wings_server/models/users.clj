(ns wings-server.models.users
  (:require [wings-server.db.sql :as sql]))

(defn create [user]
  ((sql/create-user<! user) :insert_id))

(defn count-users []
  ((first (sql/count-users)) :count))

(defn find-by-id [id]
  (first (sql/find-user-by-id {:id id})))

(defn find-by-public-id [public-id]
  (first (sql/find-user-by-public-id {:public_id public-id})))

(defn find-all []
  (sql/find-all-users))

(defn update-user [user]
  (sql/update-user! user))

(defn delete-user [id]
  (sql/delete-user! {:id id}))

(defn delete-user-with-public-id [public-id]
  (sql/delete-user-with-public-id! {:public_id public-id}))

(defn delete-all []
  (sql/delete-all-users!))
