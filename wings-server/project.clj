(defproject wings-server "0.1.0-SNAPSHOT"
  :description "Service for user profiles, events, messages"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [ring/ring-core "1.4.0"]
                 [ring/ring-jetty-adapter "1.4.0"]
                 [compojure "1.4.0"]
                 [ring/ring-json "0.4.0"]
                 [yesql "0.5.1"]
                 [environ "1.0.1"]
                 [org.mariadb.jdbc/mariadb-java-client "1.1.7"]
                 [ragtime "0.5.2"]
                 [cheshire "5.5.0"]
                 [buddy/buddy-auth "0.9.0"]]
  :main wings-server.core
; The lein-ring plugin allows us to easily start a development web server
; with "lein ring server". It also allows us to package up our application
; as a standalone .jar or as a .war for deployment to a servlet contianer
  :plugins [[lein-ring "0.8.10"]]

;ragtime command line lein migrate and rollback commands
  :aliases {"migrate"  ["run" "-m" "wings-server.core/migrate"]
            "rollback" ["run" "-m" "wings-server.core/rollback"]}
; See https://github.com/weavejester/lein-ring#web-server-options for the
; various options available for the lein-ring plugin
  :ring {:handler wings-server.handler/app
         :nrepl {:start? true
                 :port 9998}}
  
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring-mock "0.1.5"]]}
   :test {:ragtime {:database
                    "jdbc:mariadb://localhost:3306/wings_test?user=wings_test&password=pass_test"}}})

