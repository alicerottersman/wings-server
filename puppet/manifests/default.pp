exec { 'update_packages':
  command => 'apt-get update',
  path    => '/usr/bin',
}

exec{'retrieve_leiningen':
  command => "/usr/bin/wget -q https://raw.github.com/technomancy/leiningen/stable/bin/lein -O /usr/bin/lein",
  creates => "/usr/bin/lein",
}

file{'/usr/bin/lein':
  mode => 0755,
  require => Exec["retrieve_leiningen"],
}

class { '::mysql::server':}

mysql::db { 'wings_dev':
  user     => 'wings_dev',
  password => 'pass_dev',
  host     => 'localhost',
  grant    => ['ALL']
}

mysql::db { 'wings_test':
  user     => 'wings_test',
  password => 'pass_test',
  host     => 'localhost',
  grant    => ['ALL']
}

mysql::db { 'wings_auth_test':
  user     => 'wings_test',
  password => 'pass_test',
  host     => 'localhost',
  grant    => ['ALL']
}

mysql::db { 'wings_auth_dev':
  user     => 'wings_dev',
  password => 'pass_dev',
  host     => 'localhost',
  grant    => ['ALL']
}

file { "/etc/profile.d/env.sh":
  content => "export WINGS_DB_URL=\"jdbc:mariadb://localhost:3306/wings_dev?user=wings_dev&password=pass_dev\"
  export AUTH_DB_URL=\"jdbc:mariadb://localhost:3306/wings_auth_dev?user=wings_dev&password=pass_dev\""
}

include java
